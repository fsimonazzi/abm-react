import {BrowserRouter, Routes, Route} from 'react-router-dom';
import City from './components/City';
import Header from "./components/Header"
import FormularioCities from './components/FormularioCities'

function App() {
  return (
    <BrowserRouter>
     <Header />
      <div className='container mt-5'>
      <Routes>
        <Route exact path='/cities' element={<City />} />
        <Route path='/cities/new' element={<FormularioCities/>} />
      </Routes>
      </div>
    </BrowserRouter>
  )
}
export default App;



const Cities = ({city}) => {
    const {name,id,cp,state} = city

    return(
        <tr>
            <td>{id}</td>
            <td>{name}</td>
            <td>{cp}</td>
            <td>{state}</td>
        </tr>
    )
}


export default Cities
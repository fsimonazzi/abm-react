import { Fragment, useEffect, useState } from "react"
import {  Link } from "react-router-dom"
import axios from "axios"
import Cities from "./Cities"


const City= () => {
 
    
    const url= " http://localhost:4000/city"


    const [cities, setCities] = useState([])

    const getData = async () => {
        const response =await axios.get(url)
        return response
    }
    useEffect(() =>{
        getData().then((response)=>  {
            setCities(response.data)

        })
    },[])
   
    

    return(
        <Fragment>
        <h1 className="text-center">
            Cities
        </h1>
        <Link to={`/cities/new`}
           class="btn btn-primary"
        >
            New City
        </Link>
        <table class="table">
          <thead>
             <tr>
               <th scope="col">id</th>
               <th scope="col">Nombre</th>
               <th scope="col">Cp</th>
               <th scope="col">State</th>
            </tr>
         </thead>
         <tbody>
          {
            cities.length === 0 ? "no hay cities" : (
              cities.map((city) => (
              <Cities
                key={city.id}
                city={city}
                />
            ))
        )}
    
        </tbody>
      </table>
    </Fragment>                
    )
}


export default City
import { useState } from "react"


const FormularioCities = () => {

  const [nombre, guardarNombre] = useState("")
  const [cp, guardarCp] = useState()
  const [state, guardarState] = useState("")


    return(
        <form>
        <div className="form-group">
          <label htmlFor="nombre">Nombre</label>
          <input type="text"
           className="form-control"
           id="nombre"
           placeholder="Colocar nombre de la ciudad"
           name="nombre"
           value={nombre}
           onChange={(e) => guardarNombre(e.target.value)}
           >
             </input>
        </div>
        <div className="form-group">
          <label htmlFor="formGroupExampleInput2">Cp</label>
          <input type="number"
           className="form-control"
            id="cp" 
            placeholder="Coloca el codigo postal"
            value={cp}
            onChange={(e) => guardarCp(e.target.value)}
            >
            </input>
        </div>
        <div className="form-group">
          <label htmlFor="formGroupExampleInput">State</label>
          <input type="text"
           className="form-control"
            id="nombre"
            placeholder="Colocar nombre de la ciudad"
            value={state}
            onChange={(e) => guardarState(e.target.value)}
             >
             </input>
        </div>
        <input type="submit" className="btn btn-primary" value="guardar ciudad"></input>
      </form>
    )
}


export default FormularioCities
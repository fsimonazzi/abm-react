import { Link } from "react-router-dom"

const Header = () => {
    return(
        <div>
        <nav>
           <Link to={"/cities"}>cities </Link>
           <Link to={"/course"}>course </Link>
           <Link to={"/persons"}>persons </Link>
           <Link to={"/countries"}>countries</Link>
        </nav>
        </div>
    )
}


export default Header